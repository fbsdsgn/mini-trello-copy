const divFather = document.getElementById('lists')

function Lista(categoria, title, description) {
    this.categoria = categoria,
        this.title = title,
        this.description = description,

        this.divList = document.createElement('div'),
        this.divList.id = this.title,
        this.divList.textContent = this.description


    this.moveCard = function (item) {
        let boxCard = document.getElementById(`boxCard${this.title}`)
        boxCard.appendChild(item)
    },
        this.moveDiv = function () {
            let boxCard = document.createElement('div')
            boxCard.id = `boxCard${this.title}`
            this.divList.appendChild(boxCard)
            divFather.appendChild(this.divList)
        },

        this.buttonMove = function () {
            let button = document.createElement('button')
            button.textContent = 'ADD'
            button.addEventListener('click', addCard)
            this.divList.appendChild(button)
        }
}

function Card(nome, description, data) {
    this.nome = nome,
        this.description = description,
        this.data = data,

        this.divCard = document.createElement('div'),
    this.divCard.id = this.nome,
    this.divCard.textContent = this.description


}

// lista
const buttonList = document.getElementById('addList')
buttonList.addEventListener('click', addList)

function addList(){
    let form = document.getElementById('modalFormList')
    form.style.display = 'block'
}

const addListButton = document.getElementById('addButtonList')
addListButton.addEventListener('click', formImputList)

let trabalho = undefined

function formImputList() {
    let categoria = document.getElementById('categoria').value
    let description = document.getElementById('descriptionList').value
    let title = document.getElementById('titleList').value

    console.log(categoria, description, title)

    trabalho = new Lista(categoria, title, description)
    trabalho.moveDiv()
    trabalho.buttonMove()

    let form = document.getElementById('modalFormList')
    form.style.display = 'none'
}


// CARTÃO
function addCard() {
    let form = document.getElementById('modalForm')
    form.style.display = 'block'
}


const addCardButton = document.getElementById('addCard').addEventListener('click', formImput)

function formImput() {
    let title = document.getElementById('title').value
    let description = document.getElementById('description').value
    let data = document.getElementById('date').value

    let card = new Card(title, description, data)

    trabalho.moveCard(card.divCard)

    let form = document.getElementById('modalForm')
    form.style.display = 'none'
}
